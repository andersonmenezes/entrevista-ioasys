import React from 'react';

import styles from './Login.module.scss';

import logo from 'assets/logo-home.png';
import LoginInputs from 'components/Login/Inputs/Inputs';

const Login = (props: any) => {
  return (
    <div className={styles['Login']}>
      <div className={styles['form-container']}>
        <img className={styles['logo']} src={logo} alt='ioasys home logo' />
        <h4 className={styles['title']}>BEM-VINDO AO EMPRESAS</h4>
        <LoginInputs />
      </div>
    </div>
  );
};

export default Login;
