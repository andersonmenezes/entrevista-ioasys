import Company from './company.model';

export default interface State {
  enterprises: Company[];
}
