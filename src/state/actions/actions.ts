import { SAVE_ENTERPRISES } from './actiontTypes';
import Company from 'shared/models/company.model';
import axios from 'axios';

import { Dispatch } from 'redux';

export const saveEnterprise = (enterprises: Company[]) => {
  return {
    type: SAVE_ENTERPRISES,
    enterprises,
  };
};

export const searchEnterprises = (text: string) => {
  return async (dispatch: Dispatch) => {
    if (localStorage.getItem('requestHeaders')) {
      const requestHeaders: { ['access-token']: string; uid: string; client: string } = JSON.parse(
        localStorage.getItem('requestHeaders')!
      );

      try {
        const response = await axios.get(
          `https://empresas.ioasys.com.br/api/v1/enterprises/?name=${text}`,
          {
            headers: {
              'access-token': requestHeaders['access-token'],
              uid: requestHeaders.uid,
              client: requestHeaders.client,
            },
          }
        );

        dispatch(saveEnterprise(response.data.enterprises));
      } catch (error) {
        console.log(error);
      }
    }
  };
};
