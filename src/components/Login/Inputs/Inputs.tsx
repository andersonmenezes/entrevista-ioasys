import React, { useState } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import axios from 'axios';

import styles from './Inputs.module.scss';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import {
  faUnlockAlt,
  faEye,
  faEyeSlash,
  faCircleNotch,
  faExclamationCircle,
} from '@fortawesome/free-solid-svg-icons';

const LoginInputs = (props: RouteComponentProps) => {
  /** Estado do texto do input do e-mail */
  const [emailState, setEmailState] = useState('');
  /** Estado do texto do input da senha */
  const [passwordState, setPasswordState] = useState('');
  /** Gerencia quando o input da senha deve ser do tipo
   * password ou text
   */
  const [passwordVisibilityState, setPasswordVisibilityState] = useState(false);

  /** Gerencia quando a requisição está sendo enviada
   * para saber se deve ou não exibir o spinner de loading
   */
  const [requestState, setRequestState] = useState(false);
  /** Gerencia quando exibir ou não o alerta de erro. */
  const [requestErrorState, setRequestErrorState] = useState(false);

  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    if (requestErrorState === true) {
      setRequestErrorState(false);
    }
    setEmailState(value);
  };

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    if (requestErrorState === true) {
      setRequestErrorState(false);
    }
    setPasswordState(value);
  };

  const togglePasswordVisibilty = () => {
    setPasswordVisibilityState(previousState => !previousState);
  };

  const login = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setRequestState(true);

    try {
      const response = await axios.post(
        'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
        {
          email: emailState,
          password: passwordState,
        }
      );
      setRequestState(false);
      const token = response.headers['access-token'];
      const { client, uid } = response.headers;

      const userInfo = {
        'access-token': token,
        client,
        uid,
      };

      localStorage.setItem('requestHeaders', JSON.stringify(userInfo));

      props.history.push('/');
    } catch (error) {
      setRequestState(false);
      setRequestErrorState(true);
    }
  };

  const errorAppend = (
    <InputGroup.Append onClick={togglePasswordVisibilty}>
      <InputGroup.Text className={styles['append']}>
        <FontAwesomeIcon color='#ee4c77' icon={faExclamationCircle} />;{' '}
      </InputGroup.Text>
    </InputGroup.Append>
  );

  const passwordVisibilitAppend = (
    <InputGroup.Append onClick={togglePasswordVisibilty}>
      <InputGroup.Text className={[styles['append'], styles['visibility-append']].join(' ')}>
        <FontAwesomeIcon icon={passwordVisibilityState ? faEye : faEyeSlash} />
      </InputGroup.Text>
    </InputGroup.Append>
  );

  return (
    <Form
      autoComplete='off'
      onSubmit={login}
      className={['container col-12 col-md-6 col-lg-4', styles['Inputs']].join(' ')}
    >
      <InputGroup className={styles['email-input']}>
        <InputGroup.Prepend>
          <InputGroup.Text className={styles['prepend']}>
            <FontAwesomeIcon color='#ee4c77' icon={faEnvelope} />
          </InputGroup.Text>
        </InputGroup.Prepend>
        <Form.Control
          value={emailState}
          onChange={handleEmailChange}
          placeholder='E-mail'
          style={{
            background: 'transparent',
            outline: 'none',
            borderRightColor: requestErrorState ? 'transparent' : '',
          }}
          className={styles['control']}
        />
        {requestErrorState ? errorAppend : null}
      </InputGroup>

      <InputGroup className={styles['password-input']}>
        <InputGroup.Prepend>
          <InputGroup.Text className={styles['prepend']}>
            <FontAwesomeIcon color='#ee4c77' icon={faUnlockAlt} />
          </InputGroup.Text>
        </InputGroup.Prepend>
        <Form.Control
          type={passwordVisibilityState ? 'text' : 'password'}
          value={passwordState}
          onChange={handlePasswordChange}
          placeholder='Senha'
          style={{ background: 'transparent' }}
          className={[styles['control'], styles['password']].join(' ')}
        />
        {requestErrorState ? errorAppend : passwordVisibilitAppend}
      </InputGroup>
      <Button type='submit' className='col-10 mx-auto' variant='login'>
        {requestState ? <FontAwesomeIcon spin icon={faCircleNotch} /> : 'Entrar'}
      </Button>
    </Form>
  );
};

export default withRouter(LoginInputs);
