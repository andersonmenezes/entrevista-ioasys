import React from 'react';
import { Link } from 'react-router-dom';

import styles from './PreviewCard.module.scss';

import Card from 'react-bootstrap/Card';

import Company from 'shared/models/company.model';

interface PreviewCardProps {
  enterprise: Company;
}

const PreviewCard = (props: PreviewCardProps) => {
  return (
    <Link className={styles['link']} to={`${props.enterprise.id}`}>
      <Card className={styles['PreviewCard']}>
        <div className={styles['id']}>
          <h3>{`E${props.enterprise.id}`}</h3>
        </div>
        <div className={styles['card-body']}>
          <h5>{props.enterprise.enterprise_name}</h5>
          <h6>{props.enterprise.enterprise_type.enterprise_type_name}</h6>
          <small>{props.enterprise.country}</small>
        </div>
      </Card>
    </Link>
  );
};

export default PreviewCard;
