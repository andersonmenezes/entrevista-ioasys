import React from 'react';

import styles from './Nav.module.scss';

import Navbar from 'react-bootstrap/Navbar';

interface NavProps {
  children?: any;
}

const Nav = (props: NavProps) => {
  return (
    <header>
      <Navbar className={styles['Nav']} expand='lg'>
        {props.children}
      </Navbar>
    </header>
  );
};

export default Nav;
